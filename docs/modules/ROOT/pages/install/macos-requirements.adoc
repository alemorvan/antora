= macOS Installation Requirements
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
// URLs
:url-xcode: https://railsapps.github.io/xcode-command-line-tools.html
:url-nvm: https://github.com/creationix/nvm
:url-nvm-install: {url-nvm}#installation
// Versions:
:version-node: 8.10.0

On this page, you'll learn:

* [x] What tools you need in order to install Antora on macOS.
* [x] How to install the base build tools on macOS.
* [x] How to install Node 8.

If you've never installed Antora before, you need to complete the steps on this page before you can generate a documentation site.

To install Antora, you need the base build tools for macOS and Node 8.

== Base Build Tools

The {url-xcode}[Xcode command line tools^] are required for installing the nodegit package, a dependency of Antora.
This selection of tools provides other useful commands you'll need in your workflow, such as `git`.

You can trigger installation using the following command:

[source]
$ xcode-select --install

Once you have these tools installed, you can expect the remaining installation to go smoothly.

== Node 8

Antora requires Node 8, the current long term support (LTS) release of Node.
While you can try Node 9, Antora is not currently tested on it.

To check which version of Node you have installed, if any, open a terminal and type:

[source]
$ node --version

*If this command fails with an error*, you don't have Node installed.
The best way to install Node 8 is via your package manager.
However, if the package manager doesn't provide Node, skip directly to <<install-nvm,install nvm and Node>> for instructions.

*If the command returns a version less than 8.0.0*, upgrade to the latest Node 8 version using the package manager.
Or, if the package manager doesn't provide Node and you have nvm installed, go directly to <<upgrade-node,upgrade Node with nvm>> for instructions.

*If the command returns a Node 8 version*, set Node 8 as the default by typing the following command in your terminal.

[source]
$ nvm alias default 8

Now you're ready to xref:install/install-antora.adoc[install Antora].

[#install-nvm]
== Install nvm and Node

If your package manager doesn't provide Node, we recommend using the {url-nvm}[Node Version Manager (nvm)^] to manage your Node installations.
Follow these {url-nvm-install}[installation instructions^] to set up nvm on your machine.

TIP: Many CI environments use nvm to install the version of Node used for the build job.
By using nvm, you can closely align your setup with the environment that is used to generate and publish your production site.

Once you've installed nvm, open a new terminal and install Node 8.

[source]
$ nvm install 8

The above command will install the latest version of Node 8 and automatically set it as your default alias.

Now that you have Node, you're ready to xref:install/install-antora.adoc[install Antora].

[#upgrade-node]
== Upgrade Node with nvm

If you have nvm installed but your Node version is less than 8.0.0, type the following command in your terminal:

[source]
$ nvm install 8

The above command will install the latest version of Node 8.

To set the latest version of Node 8 as the default for any new terminal, type:

[source]
$ nvm alias default 8

Now that you've upgraded Node, you're ready to xref:install/install-antora.adoc[install Antora].

== What's next?

Once you've installed the base build tools and Node 8, it's time to xref:install/install-antora.adoc[install Antora].
